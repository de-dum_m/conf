#de-dum_m's oh my zsh theme

if [ $UID -eq 0 ]; then NCOLOR="red"; else NCOLOR="green"; fi
local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"

# primary prompt
PROMPT='%{$fg[yellow]%}%M%{$reset_color%}%{$fg[magenta]%} ➜ %{$reset_color%}'
# right prompt

RPROMPT='(%{$fg[cyan]%}%~%{$reset_color%})$(git_super_status)'

# git settings
ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[magenta]%}"
ZSH_THEME_GIT_PROMPT_CLEAN="$fg[green] ⛕"
ZSH_THEME_GIT_PROMPT_DIRTY="$fg[red] ⛕"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "