;; My emacs config

(load "std.el")
(load "std_comment.el")
;;(load "tuareg-site-file.el")
(if (file-exists-p "~/.myemacs") 
    (load-file "~/.myemacs"))
; EOF
(put 'upcase-region 'disabled nil)

(auto-insert-mode)
(setq auto-insert-query nil)

(setq auto-mode-alist (cons '("\.lua$" . lua-mode) auto-mode-alist))
    (autoload 'lua-mode "lua-mode" "Lua editing mode." t)

(setq vc-follow-symlinks 't)

;; Auto create headers etc for makefile and .c files
(eval-after-load 'autoinsert
  '(define-auto-insert '("\\Makefile\\'" . "Makefile")
     '(
       \n
       (my-std-file-header) \n 
       "NAME	= "
       > _ \n \n
       "SRC	= " \n \n
       "OBJ	= $(SRC:.c=.o)" \n \n
       "CFLAGS	= -Wall -Wextra -W -pedantic" \n \n
       "$(NAME): $(OBJ)" \n
       "	$(CC) $(OBJ) -o $(NAME)" \n
       "	@echo -e \"\033[032mCompiled successfully\033[0m\"" \n \n
       "all:	$(NAME)" \n \n
       "clean:" \n
       "	rm -f $(OBJ)" \n \n
       "fclean:	clean" \n
       "	rm -f $(NAME)" \n \n
       "re:	fclean all" \n \n
       ".PHONY:	all re fclean clean" \n)))

(eval-after-load 'autoinsert
  '(define-auto-insert '("\\.c\\'" . "C skeleton")
     '(
       > \n
	 (my-std-file-header) \n
	 > _ )))

(eval-after-load 'autoinsert
  '(define-auto-insert '("\\.h\\'" . "H skeleton")
     '(
       \n
       (my-std-file-header)
       \n
       "#ifndef " (upcase (file-name-sans-extension
			   (file-name-nondirectory (buffer-file-name)))) "_H_" \n
       "# define " (upcase (file-name-sans-extension
			   (file-name-nondirectory (buffer-file-name)))) "_H_" \n \n       
	 > _ \n \n
	 "#endif /* !"(upcase (file-name-sans-extension
			       (file-name-nondirectory (buffer-file-name)))) "_H_ */" \n \n)))

;; Suppression des espaces en fin de ligne a l'enregistrement
(add-hook 'c++-mode-hook '(lambda ()
			    (add-hook 'write-contents-hooks 'delete-trailing-whitespace nil t)))
(add-hook 'c-mode-hook '(lambda ()
			  (add-hook 'write-contents-hooks 'delete-trailing-whitespace nil t)))

;supprimer fichier de sauvegarde
(setq make-backup-files nil)


;; Suppression bar de menu
(menu-bar-mode -1)

;Coloriser au max
(require 'font-lock)
(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)
(setq font-lock-maximum-size nil)

;; Affiche le numero de ligne et de colonne
(column-number-mode t)
(line-number-mode t)

;; Display line numbers
;; (fringe-mode 0)
(global-linum-mode 1)
(setq linum-format "%4d \u2502 ")
(set-face-attribute 'linum nil :foreground "#0000FF")


;; Auto complete
(add-to-list 'load-path "/home/de-dum_m/.emacs.d")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "/home/de-dum_m/.emacs.d/ac-dict")
(ac-config-default)

(require 'fill-column-indicator)
(setq-default fci-rule-column 80)
(setq-default fci-rule-color "blue")
(setq-default fci-rule-character 9474)

;; C hooks
(add-hook 'c-mode-common-hook '(lambda ()
				 (show-paren-mode)
				 (set-face-foreground 'show-paren-mismatch-face "red") 
				 (set-face-attribute 'show-paren-mismatch-face nil 
						     :weight 'bold :underline t :overline nil :slant 'normal)
				 (auto-complete-mode)
				 (add-to-list 'ac-omni-completion-sources
					      (cons "\\." '(ac-source-semantic)))
				 (add-to-list 'ac-omni-completion-sources
					      (cons "->" '(ac-source-semantic)))
				 (setq yasnippet)
				 (fci-mode)))

(require 'auto-complete-clang)

(defun my-ac-config ()
  (setq-default ac-sources '(ac-source-abbrev ac-source-dictionary ac-source-words-in-same-mode-buffers))
  (add-hook 'emacs-lisp-mode-hook 'ac-emacs-lisp-mode-setup)
  ;; (add-hook 'c-mode-common-hook 'ac-cc-mode-setup)
  (add-hook 'ruby-mode-hook 'ac-ruby-mode-setup)
  (add-hook 'css-mode-hook 'ac-css-mode-setup)
  (add-hook 'auto-complete-mode-hook 'ac-common-setup)
  (global-auto-complete-mode t))
(defun my-ac-cc-mode-setup ()
  (setq ac-sources (append '(ac-source-semantic ac-source-clang ac-source-yasnippet) ac-sources)))
(add-hook 'c-mode-common-hook 'my-ac-cc-mode-setup)
;; ac-source-gtags
(my-ac-config)

(defun ac-yasnippet-candidate ()
  (let ((table (yas/get-snippet-tables major-mode)))
    (if table
      (let (candidates (list))
            (mapcar (lambda (mode)          
              (maphash (lambda (key value)    
                (push key candidates))          
              (yas/snippet-table-hash mode))) 
            table)
        (all-completions ac-prefix candidates)))))

(defface ac-yasnippet-candidate-face
  '((t (:background "sandybrown" :foreground "black")))
  "Face for yasnippet candidate.")

(defface ac-yasnippet-selection-face
  '((t (:background "coral3" :foreground "white"))) 
  "Face for the yasnippet selected candidate.")

(defvar ac-source-yasnippet
  '((candidates . ac-yasnippet-candidate)
    (action . yas/expand)
    (limit . 3)
    (candidate-face . ac-yasnippet-candidate-face)
    (selection-face . ac-yasnippet-selection-face)) 
  "Source for Yasnippet.")

(provide 'auto-complete-yasnippet)


(setq ac-clang-flags
      (mapcar (lambda (item)(concat "-I" item))
              (split-string
               "
/usr/lib/gcc/x86_64-unknown-linux-gnu/4.8.2/../../../../include/c++/4.8.2
 /usr/lib/gcc/x86_64-unknown-linux-gnu/4.8.2/../../../../include/c++/4.8.2/x86_64-unknown-linux-gnu
 /usr/lib/gcc/x86_64-unknown-linux-gnu/4.8.2/../../../../include/c++/4.8.2/backward
 /usr/lib/gcc/x86_64-unknown-linux-gnu/4.8.2/include
 /usr/local/include
 /usr/lib/gcc/x86_64-unknown-linux-gnu/4.8.2/include-fixed
 /usr/include
 ../includes
"
)))
