-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")

-- Bottom bar needs these
local vicious = require("vicious")
local blingbling = require("blingbling")
local helpers = require("blingbling.helpers")
local lain = require("lain")

-- {{{ Error handling
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init("~/.config/awesome/themes/my_theme/theme.lua")

terminal = "xfce4-terminal"
editor = os.getenv("EDITOR") or "emacs -nw"
editor_cmd = terminal .. " -e " .. editor

-- Set naughty notifications color
naughty.config.defaults.fg = '#ffffff'
naughty.config.defaults.bg = '#535d6c'


-- Startup
os.execute"run-one xcompmgr &"
os.execute"run-one gnome-screensaver &"
os.execute"run-one nm-applet &"
os.execute"run-one xfce4-power-manager &"
os.execute"run-one pidgin &"
os.execute"run-one glipper &"
os.execute"run-one pasystray &"
os.execute"run-one volnoti &"
os.execute"run-one caffeine &"
os.execute"run-one synapse -s &"
os.execute"setxkbmap -option caps:none"
os.execute"sudo /home/de-dum_m/bin/mouse_on_off"
os.execute"xmodmap -e 'keycode 135 = Super_L'&"
os.execute"numlockx &"
os.execute"run-one tilda &"
os.execute"bash /home/de-dum_m/Documents/scripts/cron/wallpaper_rotate.sh"

-- Transparency for notifications
naughty.config.presets.normal.opacity = 10
naughty.config.presets.low.opacity = 10
naughty.config.presets.critical.opacity = 10

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
layouts =
   {
      awful.layout.suit.floating,
      awful.layout.suit.tile,
      awful.layout.suit.tile.top,
      awful.layout.suit.spiral
   }
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {
     names = { "Web", "Dev", "Dev", "Dev", "Misc", "Misc", "Chat" },
     layout = { layouts[1], layouts[2], layouts[2], layouts[2], layouts[2], layouts[4], layouts[4], layouts[1] },
}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag(tags.names, s, tags.layout)
end
-- }}}

xdg_menu = require("archmenu")

mymainmenu = awful.menu({ items = { { "Poweroff", "/home/de-dum_m/Documents/scripts/interractive_power_button" },
                                    { "Applications", xdgmenu, beautiful.awesome_icon }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox
-- Create a textclock widget
mytextclock = awful.widget.textclock()

tagkeys = { "Web", "Dev", "Dev", "Dev", "Misc", "Misc", "Chat", "8", "9" }

-- Create a wibox for each screen and add it
mywibox = {}
botbox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

-- {{{ Blinbling conf


-- CPU temp
cputemp=blingbling.value_text_box({height = 18, width = 35, v_margin = 0})
cputemp:set_values_text_color({{"#88aa00ff", 0}, {"#d4aa00ff", 0.80}, {"#d45500ff", 0.90}})
cputemp:set_text_color(beautiful.textbox_widget_as_label_font_color)
cputemp:set_rounded_size(0.4)
cputemp:set_font_size(8)
cputemp:set_background_color("#00000044")
cputemp:set_label("$percent °C")
vicious.register(cputemp, vicious.widgets.thermal, "$1 ", 5, {"thermal_zone0", "sys"})

-- CPU load
cpugraph=blingbling.line_graph.new()
cpugraph:set_width(200)
cpugraph:set_rounded_size(0.4)
cpugraph:set_show_text(true)
cpugraph:set_label("CPU: $percent %")
 --bind top popup on the graph
blingbling.popups.htop(cpugraph,
       { title_color =beautiful.notify_font_color_1, 
          user_color= beautiful.notify_font_color_2, 
          root_color=beautiful.notify_font_color_3, 
          terminal = "xfce4-terminal"})
vicious.register(cpugraph, vicious.widgets.cpu, '$1', 2)

memgraph=blingbling.value_text_box({height = 18, width = 50, v_margin = 0})
memgraph:set_values_text_color({{"#88aa00ff",0}, {"#d4aa00ff", 0.75}, {"#d45500ff",0.77}})
memgraph:set_text_color(beautiful.textbox_widget_as_label_font_color)
memgraph:set_rounded_size(0.4)
memgraph:set_font_size(8)
memgraph:set_background_color("#00000044")
memgraph:set_label('RAM: $percent %')
blingbling.popups.htop(memgraph,
       { title_color =beautiful.notify_font_color_1, 
          user_color= beautiful.notify_font_color_2, 
          root_color=beautiful.notify_font_color_3, 
          terminal = "xfce4-terminal"})
vicious.register(memgraph, vicious.widgets.mem, '$1', 2)

home_fs_usage=blingbling.value_text_box({height = 18, width = 50, v_margin = 0})
home_fs_usage:set_values_text_color({{"#88aa00ff",0},
				     {"#d4aa00ff", 0.75},
				     {"#d45500ff",0.77}})
home_fs_usage:set_text_color(beautiful.textbox_widget_as_label_font_color)
home_fs_usage:set_rounded_size(0.4)
home_fs_usage:set_font_size(8)
home_fs_usage:set_background_color("#00000044")
home_fs_usage:set_label("HDD: $percent %")
vicious.register(home_fs_usage, vicious.widgets.fs, "${/ used_p}", 120 )

mynetstat=blingbling.net.new()
mynetstat:set_interface('wlo1')
mynetstat:set_height(18)
mynetstat:set_width(60)
mynetstat:set_show_text(true)
blingbling.popups.netstat(mynetstat, { title_color = "#88aa00ff", established_color= "#88aa00ff", listen_color="#88aa00ff"})

myweather=lain.widgets.yawn(615702, {u = 'c', timeout = 300})

-- }}}

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(1, mouse.screen, layouts) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1, mouse.screen, layouts) end)))

    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mylauncher)
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    if s == 1 then right_layout:add(wibox.widget.systray()) end
    right_layout:add(mytextclock)
    right_layout:add(mylayoutbox[s])

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)

    -- Create the wibox
    botbox[s] = awful.wibox({ position = "bottom", screen = s })

    local bot_left = wibox.layout.fixed.horizontal()
    bot_left:add(cpugraph)
    bot_left:add(cputemp)
    bot_left:add(memgraph)
    bot_left:add(home_fs_usage)
    bot_left:add(mynetstat)

    local bot_right = wibox.layout.fixed.horizontal()
    bot_right:add(myweather.icon)
    bot_right:add(myweather.widget)

    local bot_layout = wibox.layout.align.horizontal()
    bot_layout:set_left(bot_left)
    bot_layout:set_right(bot_right)
    
    botbox[s]:set_widget(bot_layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

function my_launch_and_move(prog, tag)
 local wait_for_it = {}

  local pid, snid = awful.util.spawn(prog, true)
  wait_for_it[snid] = { tag = awful.tag.gettags(mouse.screen)[tag] }

  client.connect_signal("manage", function (c, startup)
     if c.startup_id and wait_for_it[c.startup_id] then
        for k,v in pairs(wait_for_it[c.startup_id]) do
           c[k] = v
        end
        if wait_for_it[c.startup_id].tag then
           c:tags({wait_for_it[c.startup_id].tag})
        end
     end
  end)
end
function outputs()
      local outputs = {}
      local xrandr = io.popen("xrandr -q")
   if xrandr then
      for line in xrandr:lines() do
	 output = line:match("^([%w-]+) connected ")
	  if output then
	     outputs[#outputs + 1] = output
	      end
      end
      xrandr:close()
   end

   return outputs
end

function arrange(out)
   -- We need to enumerate all the way to combinate output. We assume
   -- we want only an horizontal layout.
      local choices  = {}
      local previous = { {} }
   for i = 1, #out do
      -- Find all permutation of length `i`: we take the permutation
      -- of length `i-1` and for each of them, we create new
      -- permutations by adding each output at the end of it if it is
      -- not already present.
      local new = {}
      for _, p in pairs(previous) do
	 for _, o in pairs(out) do
	    if not awful.util.table.hasitem(p, o) then
	       new[#new + 1] = awful.util.table.join(p, {o})
	           end
	     end
      end
      choices = awful.util.table.join(choices, new)
      previous = new
   end

   return choices
end

-- Build available choices
function menu()
      local menu = {}
      local out = outputs()
      local choices = arrange(out)

      for _, choice in pairs(choices) do
      local cmd = "xrandr"
      -- Enabled outputs
      for i, o in pairs(choice) do
	  cmd = cmd .. " --output " .. o .. " --auto"
	   if i > 1 then
	      cmd = cmd .. " --right-of " .. choice[i-1]
	       end
      end
      -- Disabled outputs
      for _, o in pairs(out) do
	 if not awful.util.table.hasitem(choice, o) then
	        cmd = cmd .. " --output " .. o .. " --off"
		 end
      end

      local label = ""
      if #choice == 1 then
	 label = 'Only <span weight="bold">' .. choice[1] .. '</span>'
      else
	 for i, o in pairs(choice) do
	        if i > 1 then label = label .. " + " end
		    label = label .. '<span weight="bold">' .. o .. '</span>'
		     end
      end

      menu[#menu + 1] = { label,
			    cmd,
			    "/usr/share/icons/Tango/32x32/devices/display.png"}
   end

   return menu
end

-- Display xrandr notifications from choices
local state = { iterator = nil,
		timer = nil,
		cid = nil }
function xrandr()
   -- Stop any previous timer
   if state.timer then
      state.timer:stop()
      state.timer = nil
   end

   -- Build the list of choices
   if not state.iterator then
      state.iterator = awful.util.table.iterate(menu(),
						function() return true end)
   end

   -- Select one and display the appropriate notification
   local next  = state.iterator()
   local label, action, icon
   if not next then
      label, icon = "Keep the current configuration", "/usr/share/icons/Faenza/devices/32/display.png"
      state.iterator = nil
   else
      label, action, icon = unpack(next)
   end
   state.cid = naughty.notify({ text = label,
				icon = icon,
				timeout = 4,
				screen = mouse.screen, -- Important, not all screens may be visible
				font = "Free Sans 18",
				replaces_id = state.cid }).id

   -- Setup the timer
   state.timer = timer { timeout = 4 }
   state.timer:connect_signal("timeout",
			      function()
				 state.timer:stop()
				      state.timer = nil
				           state.iterator = nil
					        if action then
						   awful.util.spawn(action, false)
						        end
			      end)
   state.timer:start()
end

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey, "Shift" }, "space", function () awful.layout.inc(1, mouse.screen, layouts) end),
-- Xlock
   awful.key({ modkey, "Mod1"}, "x", function () awful.util.spawn("gnome-screensaver-command -l", false) end),
-- Chrome
   awful.key({}, "#180", function () awful.util.spawn("chromium") end),
-- Calcultrice
   awful.key({}, "#148", function () awful.util.spawn("gnome-calculator") end),
-- Caps lock to activate/deactivate mouse
   awful.key({}, "#66", function () awful.util.spawn("sudo /home/de-dum_m/bin/mouse_on_off", false) end),
-- Arandr
   awful.key({modkey, }, "XF86Display", function () awful.util.spawn("arandr") end),
-- Get active outputs
   awful.key({}, "XF86Display", xrandr),

-- Switch screen
   awful.key({modkey,            }, "F1",     function () awful.screen.focus(1) end),
   awful.key({modkey,            }, "F2",     function () awful.screen.focus(2) end),

 -- bind PrintScrn to capture a screen
   awful.key(
       {},
       "Print",
       function()
           awful.util.spawn("gnome-screenshot", false)
       end
   ),
   awful.key(
       { "Shift", },
       "Print",
       function()
           awful.util.spawn("gnome-screenshot -a", false)
       end
   ),
 -- bind Ctrl + PrintScrn for gnome-screenshot interactive
   awful.key(
       { "Control", },
       "Print",
       function()
           awful.util.spawn("gnome-screenshot -i", false)
       end
   ),

   
-- Vol up/down
   awful.key({ }, "XF86AudioRaiseVolume", function ()
       awful.util.spawn("/home/de-dum_m/bin/update_vol +", false) end),
   awful.key({ }, "XF86AudioLowerVolume", function ()
       awful.util.spawn("/home/de-dum_m/bin/update_vol -", false) end),
   awful.key({ }, "XF86AudioMute", function ()
       awful.util.spawn("/home/de-dum_m/bin/update_vol m", false) end),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Change transparency
    awful.key({ modkey }, "Next", function (c)
        awful.util.spawn("transset-df --actual --inc 0.1")
    end),
    awful.key({ modkey }, "Prior", function (c)
        awful.util.spawn("transset-df --actual --dec 0.1")
    end),
	
awful.key({ modkey },       "F12",     function () 
  awful.prompt.run({ prompt = "Open: ", text = "/home/de-dum_m/code/" }, mypromptbox[mouse.screen].widget,
   function (testaus)
      my_launch_and_move("xfce4-terminal --working-directory='"..testaus.."'", 2)
      my_launch_and_move("xfce4-terminal --working-directory='"..testaus.."'", 3)
      my_launch_and_move("xfce4-terminal --working-directory='"..testaus.."'", 4)
        end,
    awful.completion.shell,
"/home/de-dum_m/.config/awesome/.open_histfile")
end),

    -- Prompt
--     awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end)
    -- Menubar
--    awful.key({ modkey }, " ", function() menubar.show() end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
-- Numpad: [0-9] = [#90, #87-#89, #83-#85, #79-#81]
local np_map = { 87, 88, 89, 83, 84, 85, 79, 80, 81 }
for i = 1, 9 do
   globalkeys = awful.util.table.join(
      globalkeys,
      awful.key({ modkey }, "#" .. np_map[i],
		function ()
           local screen = mouse.screen
           if tags[screen][i] then
              awful.tag.viewonly(tags[screen][i])
           end
		end),
      awful.key({ modkey, "Control" }, "#" .. np_map[i],
		function ()
           local screen = mouse.screen
           if tags[screen][i] then
              awful.tag.viewtoggle(tags[screen][i])
           end
		end),
      awful.key({ modkey, "Shift" }, "#" .. np_map[i],
		function ()
		   if client.focus and tags[client.focus.screen][i] then
		      awful.client.movetotag(tags[client.focus.screen][i])
           end
		end),
      awful.key({ modkey, "Control", "Shift" }, "#" .. np_map[i],
		function ()
		   if client.focus and tags[client.focus.screen][i] then
		      awful.client.toggletag(tags[client.focus.screen][i])
           end
		end))
end

for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
				       awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end),
				       awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end),
				       awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.movetotag(tag)
                          end
                     end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.toggletag(tag)
                          end
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },

    { rule = { class = "pinentry" },
      properties = { floating = true } },

    { rule = { class = "gimp" },
      properties = { floating = true } },

    { rule = { class = "pidgin" },
      properties = { floating = true } },
    { rule = { class = "Pidgin" },
      properties = { floating = true } },

    { rule = { class = "chromium" },
       properties = { tag = tags[1][1] } },
    { rule = { class = "Chromium" },
       properties = { tag = tags[1][1] } },

    { rule = { class = "Player" }, -- Genymotion VMs
       properties = { floating = true } },

    { rule = { class = "jetbrains-android-studio" },
       properties = { tag = tags[1][2] } },

    { rule = { instance = "tilda" },
       properties = { floating = true } },
    { rule = { instance = "Tilda" },
       properties = { floating = true } },

    { rule = { instance = "xv" },
       properties = { floating = true } },

    { rule = { instance = "Gpick" },
       properties = { floating = true } },
    { rule = { instance = "gpick" },
       properties = { floating = true } },

    { rule = { instance = "Terminal - irssi" },
      properties = { tag = tags[1][7] } },
    { rule = { class = "Thunderbird" },
      properties = { tag = tags[1][7] } },

    { rule = { class = "Conky" },
      properties = {
      floating = true,
      sticky = true,
      ontop = false,
      size_hints = {"program_position", "program_size"}
  } }
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align("center")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c):set_widget(layout)
    end
end)

local filter = awful.client.focus.filter ; awful.client.focus.filter = function(c) return filter(c) and c.class ~= "Conky" end

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
