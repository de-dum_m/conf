 local menue0e4fc6213e8b3593495a7260c3a4c2e = {
     {"Accerciser", "/usr/bin/accerciser", "/usr/share/icons/hicolor/16x16/apps/accerciser.png" },
     {"Screen Reader", "orca --replace", "/usr/share/icons/hicolor/16x16/apps/orca.png" },
 }

 local menu98edb85b00d9527ad5acebe451b3fae6 = {
     {"7-Zip FM", "7zFM", "/usr/share/icons/hicolor/32x32/apps/p7zip.png" },
     {"Archive Manager", "file-roller ", "/usr/share/icons/hicolor/16x16/apps/file-roller.png" },
     {"BlueProximity", "start_proximity.sh", "/usr/share/pixmaps/blueproximity.xpm" },
     {"Calculator", "gnome-calculator", "/usr/share/icons/gnome/16x16/apps/accessories-calculator.png" },
     {"Character Map", "gucharmap", "/usr/share/icons/gnome/16x16/apps/accessories-character-map.png" },
     {"Clocks", "gnome-clocks", "/usr/share/icons/hicolor/16x16/apps/gnome-clocks.png" },
     {"Desktop Search", "tracker-needle", "/usr/share/icons/gnome/16x16/actions/system-search.png" },
     {"Disks", "gnome-disks", "/usr/share/icons/hicolor/16x16/apps/gnome-disks.png" },
     {"Files", "nautilus --new-window ", "/usr/share/icons/gnome/16x16/apps/system-file-manager.png" },
     {"Font Viewer", "gnome-font-viewer ", "/usr/share/icons/gnome/16x16/apps/preferences-desktop-font.png" },
     {"IPython Qt console", "ipython qtconsole", "/usr/share/icons/gnome/16x16/status/gnome-netstatus-idle.png" },
     {"Maps", "gnome-maps", "/usr/share/icons/hicolor/16x16/apps/gnome-maps.png" },
     {"Notes", "bijiben ", "/usr/share/icons/hicolor/16x16/apps/bijiben.png" },
     {"Passwords and Keys", "/usr/bin/seahorse", "/usr/share/icons/hicolor/16x16/apps/seahorse.png" },
     {"Root Terminal", "gksu -l gnome-terminal", "/usr/share/pixmaps/gksu-root-terminal.png" },
     {"Screen Reader", "orca --replace", "/usr/share/icons/hicolor/16x16/apps/orca.png" },
     {"Screenshot", "gnome-screenshot --interactive", "/usr/share/icons/gnome/16x16/apps/applets-screenshooter.png" },
     {"Sublime Text", "sublime_text", "///opt/sublime/Icon/256x256/sublime_text.png" },
     {"Weather", "gapplication launch org.gnome.Weather.Application", "/usr/share/icons/hicolor/16x16/apps/org.gnome.Weather.Application.png" },
     {"gedit", "gedit ", "/usr/share/icons/gnome/16x16/apps/accessories-text-editor.png" },
     {"ipython", "xterm -e ipython", "/usr/share/icons/gnome/16x16/status/gnome-netstatus-idle.png" },
 }

 local menude7a22a0c94aa64ba2449e520aa20c99 = {
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
 }

 local menu251bd8143891238ecedc306508e29017 = {
     {"AisleRiot Solitaire", "sol", "/usr/share/icons/hicolor/16x16/apps/gnome-aisleriot.png" },
     {"Chess", "gnome-chess", "/usr/share/icons/hicolor/16x16/apps/gnome-chess.png" },
     {"Five or More", "five-or-more", "/usr/share/icons/hicolor/16x16/apps/five-or-more.png" },
     {"Four-in-a-row", "four-in-a-row", "/usr/share/icons/hicolor/16x16/apps/four-in-a-row.png" },
     {"Iagno", "iagno", "/usr/share/icons/hicolor/16x16/apps/iagno.png" },
     {"Klotski", "gnome-klotski", "/usr/share/icons/hicolor/16x16/apps/gnome-klotski.png" },
     {"Lights Off", "lightsoff"},
     {"Mahjongg", "gnome-mahjongg", "/usr/share/icons/hicolor/16x16/apps/gnome-mahjongg.png" },
     {"Mines", "gnome-mines", "/usr/share/icons/hicolor/16x16/apps/gnome-mines.png" },
     {"Nibbles", "gnome-nibbles", "/usr/share/icons/hicolor/16x16/apps/gnome-nibbles.png" },
     {"Quadrapassel", "quadrapassel", "/usr/share/icons/hicolor/16x16/apps/quadrapassel.png" },
     {"Robots", "gnome-robots", "/usr/share/icons/hicolor/16x16/apps/gnome-robots.png" },
     {"Sudoku", "gnome-sudoku", "/usr/share/icons/hicolor/16x16/apps/gnome-sudoku.png" },
     {"Swell Foop", "swell-foop", "/usr/share/icons/hicolor/16x16/apps/swell-foop.png" },
     {"Tali", "tali", "/usr/share/icons/hicolor/16x16/apps/tali.png" },
     {"Tetravex", "gnome-tetravex", "/usr/share/icons/hicolor/16x16/apps/gnome-tetravex.png" },
 }

 local menud334dfcea59127bedfcdbe0a3ee7f494 = {
     {"Document Viewer", "evince ", "/usr/share/icons/hicolor/16x16/apps/evince.png" },
     {"Feh", "feh ", "///usr/share/feh/images/feh.png" },
     {"GNU Image Manipulation Program", "gimp-2.8 ", "/usr/share/icons/hicolor/16x16/apps/gimp.png" },
     {"Image Viewer", "eog ", "/usr/share/icons/hicolor/16x16/apps/eog.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
 }

 local menuc8205c7636e728d448c2774e6a4a944b = {
     {"Avahi SSH Server Browser", "/usr/bin/bssh", "/usr/share/icons/gnome/16x16/devices/network-wired.png" },
     {"Avahi VNC Server Browser", "/usr/bin/bvnc", "/usr/share/icons/gnome/16x16/devices/network-wired.png" },
     {"Chromium", "chromium ", "/usr/share/icons/hicolor/16x16/apps/chromium.png" },
     {"Authy", "chromium --app-id=gaedmjdfmmahhbjefcbgaolhhanlaolb", "/usr/share/icons/hicolor/16x16/apps/authy.png" },
     {"Empathy", "empathy", "/usr/share/icons/hicolor/16x16/apps/empathy.png" },
     {"Firefox", "/usr/lib/firefox/firefox ", "/usr/share/icons/hicolor/16x16/apps/firefox.png" },
     {"Geary", "geary ", "/usr/share/icons/hicolor/16x16/apps/geary.png" },
     {"Pidgin Internet Messenger", "pidgin", "/usr/share/icons/hicolor/16x16/apps/pidgin.png" },
     {"Remote Desktop Viewer", "vinagre ", "/usr/share/icons/gnome/16x16/apps/preferences-desktop-remote-desktop.png" },
     {"Skype", "env PULSE_LATENCY_MSEC=60 skype ", "/usr/share/icons/hicolor/16x16/apps/skype.png" },
     {"Transmission", "transmission-gtk ", "/usr/share/icons/hicolor/16x16/apps/transmission.png" },
     {"Web", "epiphany ", "/usr/share/icons/gnome/16x16/apps/web-browser.png" },
     {"Zenmap", "zenmap ", "///usr/share/zenmap/pixmaps/zenmap.png" },
     {"Zenmap (as root)", "/usr/share/zenmap/su-to-zenmap.sh ", "///usr/share/zenmap/pixmaps/zenmap.png" },
 }

 local menudf814135652a5a308fea15bff37ea284 = {
     {"Dictionary", "gnome-dictionary", "/usr/share/icons/gnome/16x16/apps/accessories-dictionary.png" },
     {"Document Viewer", "evince ", "/usr/share/icons/hicolor/16x16/apps/evince.png" },
     {"Evolution", "evolution ", "/usr/share/icons/hicolor/16x16/apps/evolution.png" },
     {"LibreOffice", "libreoffice ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-startcenter.png" },
     {"LibreOffice Base", "libreoffice --base ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-base.png" },
     {"LibreOffice Calc", "libreoffice --calc ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-calc.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
     {"LibreOffice Impress", "libreoffice --impress ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-impress.png" },
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
     {"LibreOffice Writer", "libreoffice --writer ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-writer.png" },
 }

 local menue6f43c40ab1c07cd29e4e83e4ef6bf85 = {
     {"Accerciser", "/usr/bin/accerciser", "/usr/share/icons/hicolor/16x16/apps/accerciser.png" },
     {"Anjuta", "anjuta ", "/usr/share/icons/hicolor/16x16/apps/anjuta.png" },
     {"CMake", "cmake-gui ", "/usr/share/pixmaps/CMakeSetup32.png" },
     {"Database browser", "gda-browser-5.0", "/usr/share/pixmaps/gda-browser-5.0.png" },
     {"Devhelp", "devhelp", "/usr/share/icons/hicolor/16x16/apps/devhelp.png" },
     {"GHex", "ghex ", "/usr/share/icons/hicolor/16x16/apps/ghex.png" },
     {"Glade", "glade ", "/usr/share/icons/hicolor/16x16/apps/glade.png" },
     {"IPython Qt console", "ipython qtconsole", "/usr/share/icons/gnome/16x16/status/gnome-netstatus-idle.png" },
     {"KDiff3", "kdiff3  -caption \"KDiff3\"", "/usr/share/icons/hicolor/16x16/apps/kdiff3.png" },
     {"OpenJDK Monitoring & Management Console", "/usr/bin/jconsole", "/usr/share/icons/hicolor/16x16/apps/java.png" },
     {"OpenJDK Policy Tool", "/usr/bin/policytool", "/usr/share/icons/hicolor/16x16/apps/java.png" },
     {"Qt4 Assistant ", "assistant-qt4", "/usr/share/icons/hicolor/32x32/apps/assistant-qt4.png" },
     {"Qt4 Designer", "designer-qt4", "/usr/share/icons/hicolor/128x128/apps/designer-qt4.png" },
     {"Qt4 Linguist ", "linguist-qt4", "/usr/share/icons/hicolor/16x16/apps/linguist-qt4.png" },
     {"Qt4 QDbusViewer ", "qdbusviewer-qt4", "/usr/share/icons/hicolor/32x32/apps/qdbusviewer-qt4.png" },
     {"ipython", "xterm -e ipython", "/usr/share/icons/gnome/16x16/status/gnome-netstatus-idle.png" },
 }

 local menu52dd1c847264a75f400961bfb4d1c849 = {
     {"Brasero", "brasero ", "/usr/share/icons/hicolor/16x16/apps/brasero.png" },
     {"Cheese", "cheese", "/usr/share/icons/hicolor/16x16/apps/cheese.png" },
     {"Clementine", "clementine ", "/usr/share/icons/hicolor/64x64/apps/application-x-clementine.png" },
     {"Music", "gnome-music", "/usr/share/icons/hicolor/16x16/apps/gnome-music.png" },
     {"PulseAudio System Tray", "pasystray", "/usr/share/pixmaps/pasystray.png" },
     {"PulseAudio Volume Control", "pavucontrol", "/usr/share/icons/hicolor/16x16/apps/multimedia-volume-control.png" },
     {"Qt V4L2 test Utility", "qv4l2", "/usr/share/icons/hicolor/16x16/apps/qv4l2.png" },
     {"VLC media player", "/usr/bin/vlc --started-from-file ", "/usr/share/icons/hicolor/16x16/apps/vlc.png" },
     {"Videos", "totem ", "/usr/share/icons/hicolor/16x16/apps/totem.png" },
 }

 local menuee69799670a33f75d45c57d1d1cd0ab3 = {
     {"Avahi Zeroconf Browser", "/usr/bin/avahi-discover", "/usr/share/icons/gnome/16x16/devices/network-wired.png" },
     {"Disk Usage Analyser", "baobab", "/usr/share/icons/hicolor/16x16/apps/baobab.png" },
     {"GParted", "/usr/bin/gparted_polkit ", "/usr/share/icons/hicolor/16x16/apps/gparted.png" },
     {"Grub Customizer", "grub-customizer", "/usr/share/icons/hicolor/16x16/apps/grub-customizer.png" },
     {"Htop", "xterm -e htop", "/usr/share/pixmaps/htop.png" },
     {"I-Nex", "/usr/bin/i-nex", "///usr/share/pixmaps/i-nex.0.4.x.png" },
     {"Nepomuk Backup", "nepomukbackup"},
     {"Nepomuk Cleaner", "nepomukcleaner"},
     {"Network Tools", "gnome-nettool", "/usr/share/icons/hicolor/16x16/apps/gnome-nettool.png" },
     {"Oracle VM VirtualBox", "VirtualBox ", "/usr/share/icons/hicolor/16x16/mimetypes/virtualbox.png" },
     {"System Log", "gnome-system-log", "/usr/share/icons/hicolor/16x16/apps/logview.png" },
     {"System Monitor", "gnome-system-monitor", "/usr/share/icons/gnome/16x16/apps/utilities-system-monitor.png" },
     {"Terminal", "gnome-terminal", "/usr/share/icons/gnome/16x16/apps/utilities-terminal.png" },
     {"UXTerm", "uxterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
     {"VMware Player", "/usr/bin/vmplayer ", "/usr/share/icons/hicolor/16x16/apps/vmware-player.png" },
     {"VMware Workstation", "/usr/bin/vmware ", "/usr/share/icons/hicolor/16x16/apps/vmware-workstation.png" },
     {"Virtual Network Editor", "/usr/bin/vmware-netcfg", "/usr/share/icons/hicolor/16x16/apps/vmware-netcfg.png" },
     {"Wireshark", "wireshark ", "/usr/share/icons/hicolor/16x16/apps/wireshark.png" },
     {"XTerm", "xterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
     {"Xfce Terminal", "xfce4-terminal", "/usr/share/icons/gnome/16x16/apps/utilities-terminal.png" },
     {"dconf Editor", "dconf-editor", "/usr/share/icons/hicolor/16x16/apps/dconf-editor.png" },
     {"gtk2fontsel", "gtk2fontsel"},
 }

xdgmenu = {
    {"Accessibility", menue0e4fc6213e8b3593495a7260c3a4c2e},
    {"Accessories", menu98edb85b00d9527ad5acebe451b3fae6},
    {"Education", menude7a22a0c94aa64ba2449e520aa20c99},
    {"Games", menu251bd8143891238ecedc306508e29017},
    {"Graphics", menud334dfcea59127bedfcdbe0a3ee7f494},
    {"Internet", menuc8205c7636e728d448c2774e6a4a944b},
    {"Office", menudf814135652a5a308fea15bff37ea284},
    {"Programming", menue6f43c40ab1c07cd29e4e83e4ef6bf85},
    {"Sound & Video", menu52dd1c847264a75f400961bfb4d1c849},
    {"System Tools", menuee69799670a33f75d45c57d1d1cd0ab3},
}

